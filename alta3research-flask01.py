# This project must include (at least) two (2) scripts. One called alta3research-flask01.py and a second called alta3research-requests02.py

# 1.  Your script alta3research-flask01.py should demonstrate proficiency with the flask library. Ensure your application has:
# at least two endpoints
# at least one of your endpoints should return legal JSON
#!/usr/bin/env python3
from flask import Flask, make_response
from flask import render_template
from flask import jsonify
import json
from flask import request
# Flask constructor takes the name of current
# module (__name__) as argument
app = Flask(__name__)

favorite_pokemons = [{
    "name": "Butterfree",
    "number": 12,
    "pokemon_type": "Bug",
    "weaknesses": ["Fire", "Flying", "Electric", "Ice", "Rock"],
    "evolutions": [
        "Caterpie",
        "Metapod"
    ],
    "abilities": "Compound Eyes"
},
    {
        "name": "Igglybuff",
        "number": 174,
        "pokemon_type": "Fairy",
        "weaknesses": ["Steel", "Poison"],
        "evolutions": [
            "Igglybuff",
            "Jigglypuff",
            "Wigglytuff"
        ] ,
             "abilities": ["Cute Charm", "Competitive"]
             }

]


def set_cookie(i_resp, i_cnt):
    # Add a cookie to our response object
    i_resp.set_cookie("visit_cnt", i_cnt)

    # return our response object includes our cookie
    return i_resp


def get_cookie():
    # attempt to read the value of userID from user cookie
    t_visit_cnt = request.cookies.get("visit_cnt")  # preferred method

    if t_visit_cnt is None:
        t_visit_cnt = 0

    return str(t_visit_cnt)


def update_cookie():
    t_cnt = int(get_cookie())
    t_cnt = t_cnt + 1

    return str(t_cnt)

# route() function of the Flask class is a
# decorator, tells the application which URL
# should call the associated function

@app.route("/")
def get_status():
    t_cnt = update_cookie()

    t_resp = make_response(render_template("home.html", i_cnt=t_cnt))
    t_resp = set_cookie(t_resp, t_cnt)
    return t_resp
 

@app.route("/poke/<string:name>")
def verify_cool_pokemon(name):
    return render_template("index.html", pokemon=name)


@app.route("/pokemon", methods=["GET", "POST"])
def get_pokemon():
    if request.method == "POST":
        data = request.json
        if data:
            data = json.loads(data)
            name = data["name"]
            number = data["number"]
            pokemon_type = data["pokemon_type"]
            weaknesses = data["weaknesses"]
            evolutions = data["evolutions"]
            abilities = data["abilities"]
            favorite_pokemons.append(
                {"name": name, "number": number, "pokemon_type": pokemon_type, "weaknesses": weaknesses,
                 "evolutions": evolutions,"abilities":abilities})
    return jsonify(favorite_pokemons)


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=2224)

