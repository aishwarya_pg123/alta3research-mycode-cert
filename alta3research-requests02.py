"""Your script alta3research-requests02.py should demonstrate proficiency with the requests HTTP library. The API you target is up to you, but be sure any data that is returned is "normalized" into a format that is easy for users to understand."""
#!/usr/bin/env python3
from pprint import pprint
import requests
import json

URL = "http://127.0.0.1:2224/pokemon"

new_pokemon= {
    "name": "Pikachu",
    "number": 25,
    "pokemon_type": "Electric",
    "weaknesses": ["Ground"],
    "evolutions": ["Pichu","Pikachu","Raichu"],
     "abilities": "Static"
   }
#json.dumps takes a python object and returns it as a JSON string
new_pokemon= json.dumps(new_pokemon)
# requests.post requires two arguments at the minimum;
# a url to send the request
# and a json string to attach to the request
resp = requests.post(URL, json=new_pokemon)

response= requests.get(URL).json()

# pretty-print the response back from our POST request
pprint(resp.json())

for x in response:
    print(f"{x['name']} is an awesome Pokemon!  Its number is {x['number']} and its type is: {x['pokemon_type']}\n Weaknesses include {x['weaknesses']} \n Its evolutions are {x['evolutions']} and its abilities are {x['abilities']}. \n")
    

